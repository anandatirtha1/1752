The following companies and individuals have submitted contributor license agreements (CLA) and have expressed their intent on contributing code to the P1752 open source project. 
See the [Contributors](https://gitlab.com/IEEE-SA/omh/p1752/-/blob/main/CONTRIBUTORS.md) page for the list of individuals who have contributed code to the P1752 open source project to date.

* Audacious Inquiry
* Datariver SRL
* Federal Electronic Health Record Modernization Office (FEHRM)
* Humanity Innovation Labs
* Respironics, Inc.
* Vignet Inc. (dba Vibrent Health)
* Nazim Agoulmine
* Pradeep Balachandran
* Simona Carini
* Bernard Allan Cohen
* Ronak Bhadresh Gandhi
* Marissa Gray
* Paul Thomas Harris
* Shivayogi V Hiremath
* Walter Karlen
* Anandatirtha Nandugudi
* Paul Petronelli
* Banu Rekha B
* Ida Sim
