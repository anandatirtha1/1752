The following individuals have contributed code to the IEEE 1752 open source project (the list is in alphabetical order):

*	Stephanie Battista 
*	Pradeep Balachandran
*	Simona Carini
*	Charlotte Chen
*	Paul Thomas Harris
*	Shivayogi V Hiremath
*	Anandatirtha Nandugudi
*	Banu Rekha B
*	Josh Schilling
*	Ida Sim





