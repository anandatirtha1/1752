IEEE P1752 - Standard for Mobile Health Data Working Group
========================================
All contributors of code to this project must have an appropriate [Contributor License Agreement](https://opensource.ieee.org/community/cla/apache).

This repository contains IEEE standard for the minimum metadata, physical activity and mobility, and sleep measures for the [1752.1™ - Standard for Mobile Health Data](https://standards.ieee.org/standard/1752_1-2021.html). These standards are worked on by the 
[P1752 - Standard for Mobile Health Data Working Group](https://sagroups.ieee.org/1752/). More information about the P1752 family of standards for Mobile Health Data working group can be found [here](https://sagroups.ieee.org/1752/).

The files in the repository are structed as shown in the diagram below.   
```mermaid
graph LR
A(P1752)
A --> B(schemas fa:fa-link)
click B "https://gitlab.com/anandatirtha1/1752/-/tree/main/schemas" "[schemas fa:fa-link]"
A --> C(sample_data<br/><br/>each folder, except survey,<br/>contains at least one<br/>instance for each schema)
A --> D(documents)
  B --> E(environment)
  B --> F(metadata)
  B --> G(physical_activity)
  B --> H(sleep)
  B --> I(survey)
  B --> J(utility)
    E --> K[ambient-light<br/>ambient-sound<br/>ambient-temperature]
    F --> L[data-point<br/>data-series<br/>header<br/>schema-id]
    G --> M[physical-activity]
    H --> N[apnea-hypopnea-index<br/>arousal-index<br/>deep-sleep<br/>light-sleep<br/>sleep-episode<br/>sleep-onset-latency<br/>sleep-stage-summary<br/>snore-index<br/>time-in-bed<br/>total-sleep-time<br/>wake-after-sleep-onset]
    I --> O[survey<br/>survey-answer<br/>survey-categorical-answer<br/>survey-date-answer<br/>survey-item<br/>survey-question<br/>survey-time-answer<br/>survey-unit-value-answer]
  D --> P[/how to use survey schemas/]	
  C --> Q(environment)
  C --> R(metadata)
  C --> S(physical_activity)
  C --> U(survey)
  C --> V(utility)
		U --> W[4 example surveys<br/><br/>each folder contains:<br/>- text version of survey<br/>- schema of survey<br/>- 3 instances, one each for<br/>-- survey completed<br/>-- survey abandoned<br/>-- survey missed]	
		J --> X[18 schemas defining:<br/><br/>units of measure<br/>value sets<br/>time point and interval]
```

Disclaimer
-----------------

At this time, this open source repository contains material that is included in the IEEE Standard 1752.1™ 
In the future, this open source repository may also contain material that may be included in, or referenced by, an unapproved draft of a proposed IEEE Standard.
All material in this repository is subject to change. The material in this repository is presented "as is" and with all faults. Use of the material is at the sole risk of the user. IEEE specifically disclaims all warranties and representations with respect to all material contained in this repository and shall not be liable, under any theory, for any use of the material. Unapproved drafts of proposed IEEE standards must not be utilized for any conformance/compliance purposes. See the [LICENSE](LICENSE) file distributed with this work for copyright and licensing information.


License
-----------------
Copyright 2021 IEEE P1752 Authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

See the LICENSE file distributed with this work for copyright and
licensing information, the AUTHORS file for a list of copyright
holders, and the CONTRIBUTORS file for the list of contributors.

SPDX-License-Identifier: Apache-2.0
