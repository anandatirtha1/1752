{
    "$schema": "http://json-schema.org/draft-07/schema#",

    "description": "This schema models an example survey to assess poor sleep that shows how to use generic survey schemas to model question and answer pairs of various types. This survey is neither a validated instrument nor a proposed one.",
    "type": "object",

    "definitions": {
        "survey": {
            "$ref": "survey-0.1.json"
        },
        "survey_item": {
            "$ref": "survey-item-0.1.json"
        },
        "survey_categorical_answer": {
            "$ref": "survey-categorical-answer-0.1.json"
        },
        "survey_categorical_answer_item": {
            "description": "A survey item that has one categorical answer.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/survey_categorical_answer"
                            },
                            "maxItems": 1
                        }
                    }
                }
            ]
        },
        "survey_answer": {
            "$ref": "survey-answer-0.1.json"
        },
        "survey_unit_value_answer": {
            "$ref": "survey-unit-value-answer-0.1.json"
        },
        "duration_unit_value": {
            "$ref": "duration-unit-value-0.1.json"
        },
        "survey_duration_month_answer": {
            "description": "An answer that is made up of a duration value restricted to be months.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_unit_value_answer"
                },
                {
                    "properties": {
                        "value": {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/duration_unit_value"
                                },
                                {
                                    "properties": {
                                        "unit": {
                                            "enum": [
                                                "Mo"
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    }
                }
            ]
        },
        "survey_unit_value_answer_item": {
            "description": "A survey item that has one value + unit answer.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/survey_duration_month_answer"
                            },
                            "maxItems": 1
                        }
                    }
                }
            ]
        }
    },

    "allOf": [
        {
            "$ref": "#/definitions/survey"
        },
        {
            "properties": {
                "items": {
                    "type": "array",
                    "minItems": 4,
                    "items": [
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/survey_categorical_answer_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "1",
                                                    "text": "How often was your sleep disturbed last week? (Please select one below)"
                                                }
                                            ]
                                        },
                                        "answers": {
                                            "items": {
                                                "enum": [
                                                    {
                                                        "code": 0,
                                                        "value": "5 days or more"
                                                    },
                                                    {
                                                        "code": 1,
                                                        "value": "3 to 4 days"
                                                    },
                                                    {
                                                        "code": 2,
                                                        "value": "1 to 2 days"
                                                    },
                                                    {
                                                        "code": 3,
                                                        "value": "Never"
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/survey_item"
                                },
                                {
                                    "type": "object",
                                    "properties": {
                                        "answers": {
                                            "type": "array",
                                            "items": {
                                                "allOf": [
                                                    {
                                                        "$ref": "#/definitions/survey_answer"
                                                    },
                                                    {
                                                        "properties": {
                                                            "value": {
                                                                "type": "number"
                                                            }
                                                        }
                                                    }
                                                ]
                                            },
                                            "maxItems": 1
                                        }
                                    }
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "2",
                                                    "text": "How many times did you get up from bed last night after first going to sleep?"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/survey_categorical_answer_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "3",
                                                    "text": "I got adequate sleep last night."
                                                }
                                            ]
                                        },
                                        "answers": {
                                            "items": {
                                                "enum": [
                                                    {
                                                        "code": 0,
                                                        "value": "Strongly agree"
                                                    },
                                                    {
                                                        "code": 1,
                                                        "value": "Agree"
                                                    },
                                                    {
                                                        "code": 2,
                                                        "value": "Neither agree nor disagree"
                                                    },
                                                    {
                                                        "code": 3,
                                                        "value": "Disagree"
                                                    },
                                                    {
                                                        "code": 4,
                                                        "value": "Strongly disagree"
                                                    }
                                                ]
                                            }
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/survey_unit_value_answer_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "4",
                                                    "text": "How long ago did you start finding your sleep was inadequate or not refreshing?"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    "additionalItems": false
                }
            }
        }
    ]
}