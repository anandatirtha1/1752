1. How often was your sleep disturbed last week? (Please select one below)

- 5 days or more
- 3 to 4 days
- 1 to 2 days
- Never

2. How many times did you get up from bed last night after first going to sleep?

- ___ (answer is an integer)


3. I got adequate sleep last night.

- Strongly agree
- Agree
- Neither agree nor disagree
- Disagree
- Strongly disagree

4. How long ago did you start finding your sleep was inadequate or not refreshing?

- ___ months (answer is a number)

