Each folder in this directory contains:
*	The schema for an example survey that shows how to use generic survey schemas to model question and answer pairs of various types.
*	The text version of the example survey 
*	Three instances of the example survey, each showing a completed survey, an abandoned survey (i.e., not all answers provided) and a missed survey (i.e., no answer provided)

-----------------
Disclaimer
-----------------
The example surveys are neither validated instruments nor proposed ones. They are not part of the standard and are not part of the continuous integration process (i.e., not programmatically validated, not versioned).
