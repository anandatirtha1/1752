{
    "$schema": "http://json-schema.org/draft-07/schema#",

    "description": "This schema models an example survey to assess sleep related habits that shows how to use generic survey schemas to model question and answer pairs of various types. This survey is neither a validated instrument nor a proposed one.",
    "type": "object",

    "definitions": {
        "survey": {
            "$ref": "survey-0.1.json"
        },
        "survey_item": {
            "$ref": "survey-item-0.1.json"
        },
        "survey_categorical_answer": {
            "$ref": "survey-categorical-answer-0.1.json"
        },
        "survey_categorical_answer_item": {
            "description": "A survey item that has one categorical answer.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/survey_categorical_answer"
                            },
                            "maxItems": 1
                        }
                    }
                }
            ]
        },
        "yes_no_notsure_item": {
            "description": "A categorical answer item where the answer is one of three categories.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_categorical_answer_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "items": {
                                "enum": [
                                    {
                                        "code": 0,
                                        "value": "Yes"
                                    },
                                    {
                                        "code": 1,
                                        "value": "No"
                                    },
                                    {
                                        "code": 2,
                                        "value": "Not sure"
                                    }
                                ]
                            }
                        }
                    }
                }
            ]
        },
        "yes_no_item": {
            "description": "A categorical answer item where the answer is one of two categories.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_categorical_answer_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "items": {
                                "enum": [
                                    {
                                        "code": 0,
                                        "value": "Yes"
                                    },
                                    {
                                        "code": 1,
                                        "value": "No"
                                    }
                                ]
                            }
                        }
                    }
                }
            ]
        },
        "frequency_scale_item": {
            "description": "A categorical answer item where the answer is one of four categories.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_categorical_answer_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "items": {
                                "enum": [
                                    {
                                        "code": 0,
                                        "value": "Regularly"
                                    },
                                    {
                                        "code": 1,
                                        "value": "Often"
                                    },
                                    {
                                        "code": 2,
                                        "value": "Occasionally"
                                    },
                                    {
                                        "code": 3,
                                        "value": "Never"
                                    }
                                ]
                            }
                        }
                    }
                }
            ]
        },
        "survey_date_answer": {
            "$ref": "survey-date-answer-0.1.json"
        },
        "survey_date_answer_item": {
            "description": "A survey item that has an answer made up of a calendar date, complete or partial.",
            "allOf": [
                {
                    "$ref": "#/definitions/survey_item"
                },
                {
                    "type": "object",
                    "properties": {
                        "answers": {
                            "type": "array",
                            "items": {
                                "$ref": "#/definitions/survey_date_answer"
                            },
                            "maxItems": 1
                        }
                    }
                }
            ]
        }
    },
    "allOf": [
        {
            "$ref": "#/definitions/survey"
        },
        {
            "properties": {
                "items": {
                    "type": "array",
                    "minItems": 4,
                    "items": [
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/yes_no_notsure_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "1",
                                                    "text": "Have you ever taken an over-the-counter medicine to help you sleep? (Please select one below)"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/yes_no_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "2",
                                                    "text": "Do you keep track of your sleep patterns? (Please select one below)"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/frequency_scale_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "3",
                                                    "text": "Do you work night shifts?"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        },
                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/survey_date_answer_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "3.a",
                                                    "text": "When did you start working nights? (Answer is a date that can be partial)"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        },

                        {
                            "allOf": [
                                {
                                    "$ref": "#/definitions/frequency_scale_item"
                                },
                                {
                                    "properties": {
                                        "question": {
                                            "enum": [
                                                {
                                                    "label": "4",
                                                    "text": "Do you use electronic gadgets such as e-book readers, mobile phones, video game machines etc. when having difficulty in falling sleep? (Please select one below)"
                                                }
                                            ]
                                        }
                                    }
                                }
                            ]
                        }
                    ],
                    "additionalItems": false
                }
            }
        }
    ]
}