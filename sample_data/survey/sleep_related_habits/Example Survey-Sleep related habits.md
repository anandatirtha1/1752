1. Have you ever taken an over-the-counter medicine to help you sleep? (Please select one below)

- Yes
- No
- Not sure

2. Do you keep track of your sleep patterns? (Please select one below)

- Yes
- No


3. Do you work night shifts? (Please select one below)

- Regularly
- Often
- Occasionally
- Never
    - (If you have selected option “Never” then skip to question 4)
    - When did you start working nights?
    - Answer : _______ (a date that can be partial)

4. Do you use electronic gadgets such as e-book readers, mobile phones, video game machines etc. when having difficulty in falling sleep? (Please select one below)

- Regularly
- Often
- Occasionally
- Never


