1. Indicate your level of sleepiness today during the daytime (Please select one below)

- Extremely sleepy
- Very sleepy
- Moderately sleepy
- Slightly sleepy
- Not at all sleepy

2. Today, were you unable to focus on your tasks during daytime? (Please select one below)

- Very frequently
- Frequently
- Occasionally
- Rarely
- Never

3. Today, did you fall asleep in any of the following situations? (Please select all the ones that apply to you)

- During a short trip in a car
- During a short trip on public transportation
- During work in the office
- In a conversation with friends or family
- Watching TV

4. In the last 7 days, did you have difficulty participating in sport activities because you were too tired or felt sleepy? (Please select one below)

- Very frequently
- Frequently
- Occasionally
- Rarely
- Never
        
5. In the last 7 days, did you have difficulty engaging with family or friends because you were too tired or felt sleepy? (Please select one below)

- Very frequently
- Frequently
- Occasionally
- Rarely
- Never
