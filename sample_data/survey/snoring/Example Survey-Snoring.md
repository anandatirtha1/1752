1. Have other people around you noticed that you snore? (Please select one below)

- Yes
- No
- Not sure

2. In the last 6 months, have you awakened at night gasping for breath? (Please select one below)

- Very frequently
- Frequently
- Occasionally
- Rarely
- Never


3. Do you suffer from high blood pressure? (Please select one below)

- Yes
- No
- Not sure


