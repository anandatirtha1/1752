# Organization of files in this directory
- Each sample data file represents an instance of the relevant schema and complies with it (i.e., validates against it). Instances are agnostic with respect to current or future devices or apps that measure the relevant type of data.
- To allow validation, each sample data file name complies with the following template: *schema-name*-sample-dataX.json, where X can be nothing or a string, for example: ambient-sound-sample-data.json, ambient-sound-sample-data-two.json.
- With the exception of metadata, sample data files represent the content of the body of a datapoint (i.e., the sample data does not include the header, but the actual data will).
Sample instances of the schemas are structured as shown below.
```mermaid
graph LR
B(sample_data)
  B --> E(environment)
  B --> F(metadata)
  B --> G(physical_activity)
  B --> H(sleep)
  B --> I(survey)
  B --> J(utility)
    E --> K[4 instances of ambient-light<br/>3 instances of ambient-sound<br/>2 instances of ambient-temperature]
    F --> L[2 instances of data-point<br/>1 instance of data-series<br/>1 instance of header<br/>1 instance of schema-id]
    G --> M[8 instances of physical-activity]
    H --> N[2 instances of apnea-hypopnea-index<br/>2 instances of arousal-index<br/>3 instances of deep-sleep<br/>3 instances of light-sleep<br/>1 instance of sleep-episode<br/>3 instances of sleep-onset-latency<br/>4 instances of sleep-stage-summary<br/>3 instances of snore-index<br/>3 instances of time-in-bed<br/>3 instances of total-sleep-time<br/>3 instances of wake-after-sleep-onset]
    I --> O(4 example surveys<br/><br/>each folder contains:<br/>- text version of survey<br/>- schema of survey<br/>- 3 instances, one each for<br/>-- survey completed<br/>-- survey abandoned<br/>-- survey missed)
	O --> P[impact of non refreshing sleep]
	O --> Q[poor sleep assessment]
	O --> R[sleep related habits]
	O --> S[snoring]
		J --> X[1, 2 or 3 instances for each schema<br/><br/> body-posture<br/>date-time<br/>descriptive-statistic<br/>descriptive-statistic-denominator<br/>duration-unit-value<br/>duration-unit-value-range<br/>frequency-unit-value<br/>illuminance-unit-value<br/>kcal-unit-value<br/>length-unit-value<br/>percent-unit-value<br/>sound-unit-value<br/>		                   speed-unit-value<br/>time-frame<br/>time-interval<br/>unit-value<br/>unit-value-range]
```


